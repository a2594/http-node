const http = require("http");
const https = require("https");
const fs = require("fs/promises");
const path = require("path");

//Request -> (Obj) petición por parte del cliente
//Response -> (Obj) respuesta hacía el cliente

http.createServer(async(request, response) => {
    //Accediendo a la ruta que está solicitando el cliente
    const url = request.url;

    if(url === "/"){
        //Enviar una respuesta con una página de inicio
        const homePath = path.resolve("./www/index.html");
        let homePage = await getPage(homePath); //Obtenemos el contenido de la página
        response.end(homePage); //Mandamos la respuesta al cliente
    }else if(url === "/pokemon"){
        //Enviar una respuesta con una lista de pokemones
        https.get("https://pokeapi.co/api/v2/pokemon", (pokeResponse) => {
            let data = '';

            pokeResponse.on("data", (chuck) => {
                data += chuck;
            }); //Evento

            pokeResponse.on("end", () => {
                response.end(data);
            });

            //addEventListener("click")

        });
    }

}).listen(8000);

const getPage = async (path) => {
    try{
        //Vamos a leer un archivo de acuerdo a la ruta
        let data = await fs.readFile(path, "utf8");
        console.log(data);
        return data;
    }catch(error){
        console.log(error.message);
    }
}